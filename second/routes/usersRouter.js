const express = require('express');

const router = express.Router();
const {
  registerUser, loginUser, getUser, deleteUser, changePassword,
} = require('../service/usersService');
const { authMiddleware } = require('../middleware/auth');

router.post('/register', /* authMiddleware, */registerUser);
router.post('/login', /* authMiddleware, */ loginUser);

router.get('/me', authMiddleware, getUser);

router.delete('/me', authMiddleware, deleteUser);

router.patch('/me', authMiddleware, changePassword);

module.exports = {
  usersRouter: router,
};
