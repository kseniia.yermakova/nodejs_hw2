const express = require('express');

const router = express.Router();
const { authMiddleware } = require('../middleware/auth');

const {
  createNote, getNotes, getNote, deleteNote, updateNote, checkNote,
} = require('../service/notesService');

router.post('/', authMiddleware, createNote);

router.get('/', authMiddleware, getNotes);
router.get('/:id', authMiddleware, getNote);

router.delete('/:id', authMiddleware, deleteNote);

router.put('/:id', authMiddleware, updateNote);

router.patch('/:id', authMiddleware, checkNote);

module.exports = {
  notesRouter: router,
};
