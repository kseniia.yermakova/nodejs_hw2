require('dotenv').config();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../model/userModel');

const registerUser = async (req, res, next) => {
  try {
    const { name, username, password } = req.body;
    const user = new User({
      name,
      username,
      password: await bcrypt.hash(password, 10),
      createdDate: new Date(),
    });

    user.save().then(() => res.status(200).json({
      message: 'Success',
    }));
  } catch (err) {
    next({
      status: 500,
      message: err,
    });
  }
};

const loginUser = async (req, res, next) => {
  try {
    const user = await User.findOne({
      username: req.body.username,
    });
    if (
      user
      && (await bcrypt.compare(String(req.body.password), String(user.password)))
    ) {
      const payload = {
        username: user.username,
        password: user.password,
        name: user.name,
        /* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */
        userId: user._id,
      };

      const jwtToken = jwt.sign(payload, process.env.KEY);
      return res.json({
        message: 'Success',
        jwt_token: jwtToken,
      });
    }
    return res.status(400).json({
      message: 'Not athorized',
    });
  } catch (err) {
    return next({
      status: 500,
      message: err,
    });
  }
};
const getUser = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.userId);
    if (!user) {
      return next({ status: 400, message: 'Bad request' });
    }
    return res.status(200).json({
      user: {
        _id: user._id,
        username: user.username,
        createdDate: user.createdDate,
      },
    });
  } catch (err) {
    return next({
      status: 500,
      message: err,
    });
  }
};

const deleteUser = async (req, res, next) => {
  try {
    const user = await User.findByIdAndDelete(req.user.userId);
    if (!user) {
      return next({ status: 400, message: 'Bad request' });
    }
    return res.status(200).json({
      message: 'Success',
    });
  } catch (err) {
    return next({
      status: 200,
      message: err,
    });
  }
};

const changePassword = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.userId);
    if ((await bcrypt.compare(req.body.oldPassword, user.password)) === false) {
      res.status(400).send({ message: 'Old password is incorrect' });
    } else {
      user.password = await bcrypt.hash(req.body.newPassword, 10);
      await user.save();
      res.status(200).json({
        message: 'Success',
      });
    }
  } catch (err) {
    next({
      status: 200,
      message: err,
    });
  }
};

module.exports = {
  registerUser,
  loginUser,
  getUser,
  deleteUser,
  changePassword,
};
