const { Note } = require('../model/notesModel');

// done + add userId
const createNote = async (req, res, next) => {
  const { text } = req.body;
  const { userId } = req.user;
  const createdDate = new Date();
  const note = new Note({
    text,
    userId,
    createdDate,
  });

  return note.save().then(() => {
    res.status(200).json({
      message: 'Success',
    });
  }).catch((err) => {
    next({ status: 500, message: err });
  });
};

const getNotes = async (req, res) => {
  const { offset = 0, limit = 0 } = req.query;
  const notes = await Note.find({ userId: req.user.userId }).skip(offset).limit(limit);
  return res.status(200).json({
    offset,
    limit,
    count: notes.length,
    notes,
  });
};
const getNote = async (req, res, next) => {
  try {
    const note = await Note.findById(req.params.id);
    if (!note) {
      next({ status: 400, message: 'Bad request' });
    } else {
      res.status(200).json({
        note,
      });
    }
  } catch (err) {
    next({
      status: 500,
      message: err,
    });
  }
};

const deleteNote = async (req, res, next) => {
  try {
    const note = await Note.findByIdAndDelete(req.params.id);
    if (!req.params.id) {
      return next({ status: 400, message: 'Bad request' });
    }
    return res.status(200).json({
      message: `Success ${note} deleted`,
    });
  } catch (err) {
    return next({
      status: 500,
      message: err,
    });
  }
};

const updateNote = async (req, res, next) => {
  try {
    const note = await Note.findById(req.params.id);
    if (!note) {
      next({
        status: 400,
        message: 'Bad request',
      });
    } else {
      note.text = req.body.text;
      note.save().then(() => {
        res.status(200).json({
          message: 'Success',
        });
      });
    }
  } catch (err) {
    next({
      status: 500,
      message: err,
    });
  }
};

const checkNote = async (req, res, next) => {
  try {
    const note = await Note.findById(req.params.id);
    if (!note) {
      next({
        status: 400,
        message: 'Bad request',
      });
    } else {
      note.completed = !note.completed;
      note.save().then(() => {
        res.status(200).json({
          message: 'Success',
        });
      });
    }
  } catch (err) {
    next({
      status: 500,
      message: err,
    });
  }
};

module.exports = {
  createNote,
  getNotes,
  getNote,
  deleteNote,
  updateNote,
  checkNote,
};
