require('dotenv').config();
const express = require('express');

const app = express();
const morgan = require('morgan');

const mongoose = require('mongoose');

mongoose.connect(`mongodb+srv://${process.env.DB_OWNER}:${process.env.DB_PASSWORD}@cluster0.q8tc1ea.mongodb.net/test`);

const { usersRouter } = require('./second/routes/usersRouter');
const { notesRouter } = require('./second/routes/notesRouter');

app.use(express.json());
app.use(morgan('tiny'));
// pracye

app.use('/api/auth', usersRouter);

app.use('/api/users', usersRouter);
app.use('/api/notes', notesRouter);

// set port, start server
const { PORT } = process.env;
const start = async () => {
  try {
    app.listen(PORT);
  } catch (err) {
    console.error(`Error on server startup ${err.message}`);
  }
};

start();

const errorHandler = (err, req, res) => {
  console.error('err');
  res.status(err.status).send({ message: err.message });
};

app.use(errorHandler);
